"use strict";

/*console.log('Welcome to ', {title:'PizzaLand', emoji: '🍕'});
console.log(42+"12"-10);

let what = 'door';
console.log('Hold','the',what);*/
var name = "Regina"; //const data = [`Regina`,`Napolitaine`,`Spicy`];

var data = [{
  name: 'Regina',
  base: 'tomate',
  price_small: 6.5,
  price_large: 9.95,
  image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
}, {
  name: 'Napolitaine',
  base: 'tomate',
  price_small: 6.5,
  price_large: 8.95,
  image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
}, {
  name: 'Spicy',
  base: 'crème',
  price_small: 5.5,
  price_large: 8,
  image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300'
}];
var url = "images/".concat(name.toLowerCase(), ".jpg");
var html = "";
console.log(url);
data.forEach(function (element) {
  var url2 = "".concat(element.image);
  html = "".concat(html, "\n<article class=\"pizzaThumbnail\">\n<a href=\"").concat(url2, "\">\n<img src=\"").concat(url2, "\"/>\n<section>\n<h4>").concat(element.name, "</h4>\n<ul>\n<li>Prix petit format : ").concat(element.price_small, "\u20AC</li>\n<li>Prix grand format : ").concat(element.price_large, "\u20AC</li>\n</ul>\n</section>\n</a>\n</article>");
  document.querySelector('.pageContent').innerHTML = html;
});
console.log(html);