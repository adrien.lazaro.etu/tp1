/*console.log('Welcome to ', {title:'PizzaLand', emoji: '🍕'});
console.log(42+"12"-10);

let what = 'door';
console.log('Hold','the',what);*/

let name = `Regina`;
//const data = [`Regina`,`Napolitaine`,`Spicy`];
let data = [
	{
		name: 'Regina',
		base: 'tomate',
		price_small: 6.5,
		price_large: 9.95,
		image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	},
	{
		name: 'Napolitaine',
		base: 'tomate',
		price_small: 6.5,
		price_large: 8.95,
		image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
	},
	{
		name: 'Spicy',
		base: 'crème',
		price_small: 5.5,
		price_large: 8,
		image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
	}
]
/*
data.sort(function (a,b){
    return a.name.localeCompare(b.name);
});
*/
/*
data.sort(function (a,b){
    return a.price_small-b.price_small;
})*/
/*
data.sort(function (a,b){
    if(a.price_small-b.price_small != 0){
        return a.price_small-b.price_small;
    }else{
        return a.price_large-b.price_large;
    }
})
*/

//data = data.filter(pizza => pizza.base.localeCompare("tomate")==0);
//data = data.filter(pizza => pizza.price_small<6);
//let RegEx = /.*i.*i.*/
//data = data.filter(pizza => pizza.name.match(RegEx))

const url = `images/${name.toLowerCase()}.jpg`;
let html = ``;
console.log(url);
data.forEach(({name, image, price_small, price_large}) => {
const url2 = `${image}`;
html = `${html}
<article class="pizzaThumbnail">
<a href="${url2}">
<img src="${url2}"/>
<section>
<h4>${name}</h4>
<ul>
<li>Prix petit format : ${price_small}€</li>
<li>Prix grand format : ${price_large}€</li>
</ul>
</section>
</a>
</article>`;
document.querySelector('.pageContent').innerHTML = html;
});
console.log(html);
